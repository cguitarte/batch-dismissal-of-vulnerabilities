from envparse import Env
#from gitlab import Gitlab as GitLab
import requests
import json
import os


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')



resolve_mutation = """mutation vulnerabilityResolve($vulnerability_id: VulnerabilityID!) {
    vulnerabilityResolve(input: { id: $vulnerability_id}) {
        vulnerability {
            state
        }
        errors
    }
}"""

def main():
    project_path = env('PROJECT_PATH')
    proj_var = {"project": project_path}
    has_next_page = True
    after_cursor = None
    
    # Query through page of 100 items, loop through if hasNextPage returns for graphql
    while has_next_page:
      vulnerability_findings_query = createFindingsQuery(after_cursor)
      findings_res = request_gql(vulnerability_findings_query, proj_var)
      has_next_page = findings_res["data"]["project"]["vulnerabilities"]["pageInfo"]["hasNextPage"]
      after_cursor = findings_res["data"]["project"]["vulnerabilities"]["pageInfo"]["endCursor"]
      vulnerability_findings = findings_res["data"]["project"]['vulnerabilities']['nodes']
      for vulnerability in vulnerability_findings:
        id = vulnerability['id']
        id_var = {"vulnerability_id": id}
        resolve_res = request_gql(resolve_mutation, id_var)
        print(f'Vulnerability ID ${id} has been marked resolved: ${resolve_res}')


#States: state: [DETECTED,CONFIRMED,DISMISSED, RESOLVED]
def createFindingsQuery(after_cursor=None):
  return """query getVulnerabilities($project: ID!) {
    project(fullPath: $project){
      name
      vulnerabilitySeveritiesCount(state: [DETECTED, CONFIRMED,DISMISSED]){
        critical
        high
        medium
        low
        info
        unknown
      }
      vulnerabilities(first: 100, after: AFTER, state: [DETECTED, CONFIRMED,DISMISSED]){
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes{
          id
          title
        }
      }
    }
  }""".replace(
          "AFTER", '"{}"'.format(after_cursor) if after_cursor else "null"
      )

def request_gql(query, variables):
    request_url = env('GITLAB_HOST_GRAPHQL')
    private_token = env('PRIVATE_TOKEN')
    request = requests.post(request_url, json={'query': query, 'variables': variables}, headers={'PRIVATE-TOKEN': private_token})
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception("Failed to run by returning code of {}. {}".format(request.status_code, query))


if __name__ == '__main__':
    main()
