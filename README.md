# Automatically Resolve All Vulnerability Findings
This is a script to mark all vulnerabilities resolved for a single project. This is in rough draft form and has not been implemented with a dry run or confirmation checks. Please use with caution.

Variables
 * PRIVATE_TOKEN: Personal access token of GitLab Environment with API Write Access
 * GITLAB_HOST_GRAPHQL=https://gitlab.com/api/graphql/ or self-managed graphql enpoint
 * PROJECT_PATH: Full path of group/project (ex: group/subgroup/test-project)


To run:
1. Fill in environment variables above in a `.env` file.
2. Install dependencies. `pip3 install -r requirements.txt > /dev/null`
3. Run command `python3 resolve_vulnerabilities.py`

Vulnerability Status: https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#vulnerability-status-values 

APIs Used:
* https://docs.gitlab.com/ee/api/vulnerabilities.html#graphql---resolve-vulnerability 
* https://docs.gitlab.com/ee/api/vulnerability_findings.html#graphql---project-vulnerabilities 

